#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include "node.h"

extern int errno;

struct _Node {
	char name[100];
	int id;
	int nConnect;
	Label etq;
};

Label node_getLabel(const Node *nodo){
	if(nodo==NULL)
	  return BLANCO;
	else
	  return(nodo->etq);
}

Node* node_setLabel(Node *nodo, const int tipo){
	if(nodo==NULL)
	  return NULL;
  if(tipo==0){
	  nodo->etq=BLANCO;
		return nodo;
	}
  if(tipo==1){
	  nodo->etq=NEGRO;
    return nodo;
	}
	return nodo;
}

Node *node_ini(){
	Node *node;
	node = (Node *) malloc(sizeof(Node));

	if(node==NULL){
		fprintf(stderr, "Value of errno: %d\n", errno);
		fprintf(stderr, "Error reserving memory: %s\n", strerror(errno));
		return NULL;
	};

	node->name[0] = '\0';
	node->id = 0;
	node->nConnect= 0;
	node->etq=BLANCO;

	return node;
}

void node_destroy(Node *n){
	free(n);
}

int node_getId(const Node *n){
	if(n==NULL) return -1;
	return(n->id);
}

char * node_getName(const Node *n){
	if(n==NULL) return NULL;
	return (char *) n->name;
}

int node_getConnect(const Node * n){
	if(n==NULL) return -1;
	return (n->nConnect);
}

Node *node_setId(Node * n, const int id){
	if(n==NULL) return NULL;
	n->id=id;
	return n;
}

Node *node_setName(Node * n, const char* name){
	if(n==NULL) return NULL;
	strcpy(n->name, name);
	return n;
}

Node *node_setConnect(Node * n, const int cn){
	if(n==NULL) return NULL;
	n->nConnect=cn;
	return n;
}

int node_cmp (const Node * n1, const Node * n2){

	int i;

	if (n1->id==n2->id)
		return 0;
	else if (n1->id<n2->id)
		return -1;
	else
		return 1;

	i=strcmp(n1->name, n2->name);
	return i;

} /*No se que hacer con lo de comparar nombres*/

Node *node_copy(const Node * src){
	Node *n;
	char *name;
	int id;
	int nConnect;
	Label etiq;
	n=node_ini();


	id=node_getId(src);
	nConnect=node_getConnect(src);
	name=node_getName(src);
	etiq = node_getLabel(src);

	n=node_setId(n, id);
	n=node_setName(n, name);
	n=node_setConnect(n, nConnect);
  n=node_setLabel(n, etiq);


	if(n==NULL) return NULL;
	return n;
}

int node_print(FILE *pf, const Node * n){
	int caracteres = 0;

	caracteres = fprintf(pf, "[%d, %s, %d] ", n->id, n->name, n->nConnect);
	if (n->etq == BLANCO){
		caracteres = fprintf(pf, "BLANCO\n") + caracteres;
	}
	else{
		caracteres = fprintf(pf, "NEGRO\n") + caracteres;
	}

	if(n==NULL){
		fprintf(stderr, "Value of errno: %d\n", errno);
		fprintf(stderr, "Error opening file: %s\n", strerror(errno));
	};
	return caracteres;
}
