#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include "graph.h"
#include "types.h"

extern int errno;

#define MAX_NODE 4096
#define MAX_LINE 100
#define NO_FILE_POS_VALUES 2


struct _Graph {
	Node *nodes[MAX_NODE];
	int num_nodes;
	Bool connections[MAX_NODE][MAX_NODE];
};

/*********************************/
/*      Funciones privadas       */
/*********************************/

/*
 * Busca en el grafo recibido el nodo con la id recibida y debuelve su
 * posicion, -1 en caso de error;
 */
int find_node_index(const Graph * g, int nId1);

/*
 * Retorna un puntero a un array de enteros con las posiciones de los nodos
 * con los que esta conectado el nodo de la posicion recibida.
 */
int* graph_getConectionsIndex(const Graph * g, int index);

void destroy_node_function(void* e);
void * copy_node_function(const void* e);
int print_node_function(FILE * f, const void* e);


/*********************************/
/*  Implementacion de funciones  */
/*********************************/

int find_node_index(const Graph * g, int nId1) {
	int i;
	if (!g) return -1;
	for(i=0; i < g->num_nodes; i++) {
		if (node_getId (g->nodes[i]) == nId1) return i;
	}

	/* ID not find */
	return -1;
}

int* graph_getConectionsIndex(const Graph * g, int index) {
	int *array = NULL, i, j=0, size;

	if (!g) return NULL;
	if (index < 0 || index >g->num_nodes) return NULL;

	/* get memory for the array with the connected nodes index */
	size = node_getConnect (g->nodes[index]);
	array = (int *) malloc(sizeof(int) * size);
	if (!array) {
	/* print errorr message */
		fprintf (stderr, "%s\n", strerror(errno));
		return NULL;
	}
	/* asigno valores al array con los indices de los nodos conectados */
	for(i = 0; i< g->num_nodes; i++) {
		if (g->connections[index][i] == TRUE) {
			array[j] = i;
			j++;
		}
	}
	return array;
}

void destroy_node_function(void* e){
    node_destroy((Node *)e);
}

void * copy_node_function(const void* e){
    return node_copy((Node *)e);
}

int print_node_function(FILE * f, const void* e){
    return node_print(f, (Node *)e);
}

/*----------------------------------------------------------------------------*/

Graph * graph_ini() {
	Graph *new_Graph = NULL;
	int i, j;
	new_Graph = (Graph *) malloc (sizeof(Graph));
	if (new_Graph == NULL) {
		return NULL;
	}
	for (i=0; i < MAX_NODE; i++){
		new_Graph->nodes[i] = NULL;
	}
	new_Graph->num_nodes = 0;
	for (i=0; i < MAX_NODE; i++){
		for (j=0; j < MAX_NODE; j++){
			new_Graph->connections[i][j] = FALSE;
		}
	}
	return new_Graph;
}

void graph_destroy(Graph * g){
	int i;
	for(i = 0; g->nodes[i] != NULL; i++){
		free(g->nodes[i]);
	}
	free(g);
}

Status graph_insertNode(Graph * g, const Node* n){
	int id;
	if(g == NULL || n == NULL){
		return ERROR;
	}
	id = node_getId(n);
	if (find_node_index(g, id) == -1){
		g->nodes[g->num_nodes] = node_copy(n);
		g->num_nodes++;
		return OK;
	}
	return ERROR;
}



Status graph_insertEdge(Graph * g, const int nId1, const int nId2){
	int fila, columna, num_connect;
	if (g == NULL){
		return ERROR;
	}
	fila = find_node_index(g, nId1);
	columna = find_node_index(g, nId2);
	if (fila == -1 || columna == -1){
		return ERROR;
	}
	g->connections[fila][columna] = TRUE;
	num_connect = node_getConnect(g->nodes[fila]);
	if (num_connect == -1){
		return ERROR;
	}
	num_connect ++;
	g->nodes[fila] = node_setConnect(g->nodes[fila], num_connect);
	if (g->nodes[fila] == NULL){
		return ERROR;
	}
	return OK;
}

Node *graph_getNode (const Graph *g, int nId){
	int position;
	if (g == NULL){
		return NULL;
	}
	position = find_node_index(g, nId);
	if (position == -1){
		return NULL;
	}
	return node_copy(g->nodes[position]);
}

Status graph_setNode (Graph *g, const Node *n){
	int id, position;
	if(g == NULL || n == NULL){
		return ERROR;
	}
	id = node_getId(n);
	if (id == -1){
		return ERROR;
	}
	position = find_node_index(g, id);
	if (position == -1){
		return ERROR;
	}
	node_destroy(g->nodes[position]);
	g->nodes[position] = node_copy(n);
	if(g->nodes[position] == NULL){
		return ERROR;
	}
	return OK;
}



int * graph_getNodesId (const Graph * g){
	int i;
	int *array;
	if (g == NULL){
		return NULL;
	}
	array = (int *) calloc (g->num_nodes - 1, sizeof(int));
	if (array == NULL){
		return NULL;
	}
	for (i=0; i < g->num_nodes; i++){
		array[i] = node_getId(g->nodes[i]);
	}
	return array;
}

int graph_getNumberOfNodes(const Graph * g){
	if (g == NULL){
		return -1;
	}
	return g->num_nodes;
}

int graph_getNumberOfEdges(const Graph * g) {
	int num_aristas = 0, i, j, rango;
	if (g == NULL){
		return -1;
	}
	rango = g->num_nodes - 1;
	for (i=0; i <= rango; i++){
		for (j=0; j <= rango; j++){
			if (g->connections[i][j] == TRUE){
				num_aristas++;
			}
		}
	}
	return num_aristas;
}



Bool graph_areConnected(const Graph * g, const int nId1, const int nId2){
	int fila, columna;
	if (g == NULL){
		return ERROR;
	}
	fila = find_node_index(g, nId1);
	columna = find_node_index(g, nId2);
	if (fila == -1 || columna == -1){
		return ERROR;
	}
	if (g->connections[fila][columna] == TRUE) {
		return OK;
	}
	return ERROR;
}

int graph_getNumberOfConnectionsFrom(const Graph * g, const int fromId){
	int connections, position;
	if (g == NULL){
		return -1;
	}
	position = find_node_index(g, fromId);
	if (position == -1){
		return -1;
	}
	connections = node_getConnect(g->nodes[position]);
	return connections;
}

int* graph_getConnectionsFrom(const Graph * g, const int fromId){
	int *nodes_id, *nodes_position;
	int num_connect, position, i;
	if (g == NULL){
		return NULL;
	}
	num_connect = graph_getNumberOfConnectionsFrom(g, fromId);
	if (num_connect < 0) {
		return NULL;
	}

	position = find_node_index(g, fromId);
	if (position == -1) {
		return NULL;
	}

	nodes_id = (int *) calloc ((num_connect), sizeof(int));
	if (nodes_id == NULL){
		return NULL;
	}

	nodes_position = graph_getConectionsIndex(g, position);
	if (nodes_position == NULL){
		return NULL;
	}

	for (i = 0; i < num_connect; i++){
		nodes_id[i] = node_getId(g->nodes[nodes_position[i]]);
	}

	free(nodes_position);
	return nodes_id;
}



int graph_print(FILE *pf, const Graph * g) {
	int *ids = NULL;
	int caracteres, num_connect, id, i, j;
	for (i=0, caracteres = 0; i < g->num_nodes; i++) {
		caracteres += node_print(pf, g->nodes[i]);
		num_connect = node_getConnect(g->nodes[i]);

		if (num_connect > 0) {
			id = node_getId(g->nodes[i]);
			ids = graph_getConnectionsFrom(g, id);
			if (ids == NULL) {
        fprintf (stderr, "%s\n", strerror(errno));
        return -1;
      }
			for (j=0; j < num_connect; j++) {
				 caracteres += fprintf(pf,"%d",ids[j]);

			}
			free(ids);
		}
		fprintf(pf,"\n");
	}
	return caracteres;
}

Status graph_readFromFile (FILE *fin, Graph *g) {

    Node *n;
    char buff[MAX_LINE], name[MAX_LINE];
    int i, nnodes = 0, id1, id2;
    Status flag = ERROR;

    /* read number of nodes */
    if ( fgets (buff, MAX_LINE, fin) != NULL)

        if ( sscanf(buff, "%d", &nnodes) != 1) return ERROR;
    /* init buffer_node */
    n = node_ini();
    if (!n) return ERROR;

    /* read nodes line by line */
    for(i=0; i < nnodes; i++) {
        if ( fgets(buff, MAX_LINE, fin) != NULL)
            if (sscanf(buff, "%d %s", &id1, name) != NO_FILE_POS_VALUES) break;
        /* set node name and node id */
        node_setName (n, name);
        node_setId (n, id1);
        /* insert node in the graph */
       if ( graph_insertNode (g, n) == ERROR) break;
    }
    /* Check if all node have been inserted */
    if (i < nnodes) {
        node_destroy(n);
        return ERROR;
    }
    /* read connectionss line by line and insert it */
    while ( fgets(buff, MAX_LINE, fin)) {
	if ( sscanf(buff, "%d %d", &id1, &id2) == NO_FILE_POS_VALUES )
	    if (graph_insertEdge(g, id1, id2) == ERROR) break;
    }

    /* check end of file */
    if (feof(fin)) flag = OK;
    /* clean up, free resources */
    node_destroy (n);
    return flag;
}

int graph_findDeepSearch (Graph *g, int from, int to){
	Queue *cola = NULL; /*Cola donde se guardan los nodos que se van revisando*/
	Node *node_temp = NULL; /*Nodo temporal*/
	Node *destino = NULL; /*Nodo destino*/

	Node *nodo_auxiliar = NULL; /*Nodo auxiliar usado para revisar los nodos de la cola principal*/

	int all; /*Contador con la cantidad de nodos de la cola principal*/
	int *lista; /*Lista con las id del los nodos conectados al nodo que se revisa*/
	int conect = 0; /*Cantidad de nodos conectados al nodo que se revisa*/
	int i; /* contador*/
	int size = 0; /*Tama�o de la cola */
	int equal = 0; /* Se�al que indica que si se ha encontrado un nodo igual al que se
									quiere introcucir en la cola */


	if (g == NULL || from < 1 || to < 1) return 0;
	if (from == to){
		return 1;
	}

	node_temp = graph_getNode(g, from);
	if(node_temp == NULL) return 1;

	destino = graph_getNode(g, to);
	if(destino == NULL){
		node_destroy(node_temp);
		return 1;
	}


	cola = queue_ini(destroy_node_function, copy_node_function, print_node_function);
	if(cola == NULL){
		node_destroy(node_temp);
		node_destroy(destino);
		return 1;
	}

	queue_insert(cola, node_temp);

	node_destroy(node_temp);
	all = queue_size(cola);


	while (all != 0){
		node_temp = queue_extract(cola);
		if (node_getLabel(node_temp) == BLANCO){
			node_setLabel(node_temp, NEGRO);
			lista = graph_getConnectionsFrom(g, node_getId(node_temp));
			conect = node_getConnect(node_temp);
			queue_insert(cola, node_temp);
			node_destroy(node_temp);
			for(i=0; i<conect; i++){
				node_temp = graph_getNode(g, lista[i]);
				if (node_cmp(node_temp, destino) == 0){
					node_destroy(node_temp);
					queue_destroy(cola);
					node_destroy(destino);
					free(lista);
					return 1;
				}
				else{
					size = queue_size(cola);
					while(size>0){
						nodo_auxiliar = queue_extract(cola);
						if(node_cmp(nodo_auxiliar,node_temp) == 0){
							equal = 1;
						}
						queue_insert(cola, nodo_auxiliar);
						node_destroy(nodo_auxiliar);
						size--;
					}
					if(equal == 0){
						node_setLabel(node_temp, BLANCO);
						queue_insert(cola, node_temp);
					}
					node_destroy(node_temp);
					equal = 0;
				}
			}
			free(lista);
			all = queue_size(cola);
		}
		else{
			queue_insert(cola, node_temp);
			node_destroy(node_temp);
			all--;
		}
	}

	node_destroy(destino);
	queue_destroy(cola);
  return 0;
}
