
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include "list.h"


typedef struct _NodeList { /* EdD privada, necesaria para implementar lista */
	void* info;
	struct _NodeList *next;
} NodeList;                /* Tipo NodeList privado */

struct _List {
	NodeList *last;  /*La LEC apunta al último nodo y el último al primero*/
	destroy_element_function_type destroy_element_function;
	copy_element_function_type copy_element_function;
	print_element_function_type print_element_function;
	cmp_element_function_type cmp_element_function;
};



/* Inicializa la lista reservando memoria e in
icializa todos sus elementos.
*/
List* list_ini(
    destroy_element_function_type f1,
    copy_element_function_type f2,
    print_element_function_type f3,
    cmp_element_function_type f4
){
	List *list=NULL;
	list=(List*)malloc(sizeof(List));
	if(list==NULL){
	  return NULL;}
	list->destroy_element_function=f1;
	list->copy_element_function=f2;
	list->print_element_function=f3;
	list->cmp_element_function=f4;
	list->last=NULL;
	return list;
}

/* Libera la lista, liberando todos sus elementos.
*/
void list_destroy(List* list){
	NodeList *pNodeList=NULL;
	NodeList *pNodeList_auxiliar=NULL;
	if(list==NULL){
	  return;}
	if(list->last==NULL){
		free(list);
		return;
	}
	list->destroy_element_function(list->last->info);
	pNodeList_auxiliar=list->last;
	pNodeList=list->last->next;
	list->last->next=NULL;
	free(pNodeList_auxiliar);
	while(pNodeList!=NULL){
		list->destroy_element_function(pNodeList->info);
		pNodeList_auxiliar=pNodeList;
		pNodeList=pNodeList->next;
		pNodeList_auxiliar->next=NULL;
		free(pNodeList_auxiliar);
	}
	free(list);
}

/* Inserta al principio de la lista realizando una copia de la información recibida.
Devuelve NULL en caso de error o la lista si todo OK */
List* list_insertFirst(List*list, const void *pelem){
	NodeList *pNodeList=NULL;
	NodeList *pNodeList_next=NULL;
	if(list==NULL || pelem==NULL)
	  return NULL;
	if(list->last==NULL){
		pNodeList=(NodeList*)malloc(sizeof(NodeList));
	  if(pNodeList==NULL){
		  return NULL;}
		pNodeList->info=(list->copy_element_function(pelem));
		pNodeList->next=pNodeList;
		list->last=pNodeList;
		return list;
	}
	pNodeList_next=list->last->next;
	pNodeList=(NodeList*)malloc(sizeof(NodeList));
	if(pNodeList==NULL){
	  return NULL;}
	pNodeList->next=pNodeList_next;
	pNodeList->info=(list->copy_element_function(pelem));
	list->last->next=pNodeList;
	return list;
}

/* Inserta al final de la lista realizando una copia de la información recibida.
Devuelve NULL en caso de error o la lista si todo ha salido bien*/
List*list_insertLast (List* list, const void *pelem){
	NodeList *pNodeList=NULL;
	NodeList *pNodeList_next=NULL;
	if(list==NULL || pelem==NULL)
	  return NULL;
	if(list->last==NULL){
			pNodeList=(NodeList*)malloc(sizeof(NodeList));
			if(pNodeList==NULL){
			  return NULL;}
			pNodeList->info=(list->copy_element_function(pelem));
			pNodeList->next=pNodeList;
			list->last=pNodeList;
			return list;
		}
	pNodeList=(NodeList*)malloc(sizeof(NodeList));
	if(pNodeList==NULL)
	  return NULL;
	pNodeList->info=list->copy_element_function(pelem);
	pNodeList_next=list->last->next;
	pNodeList->next=pNodeList_next;
	list->last->next=pNodeList;
	list->last=pNodeList;
  return list;
}

/* Inserta en orden en la lista realizando una copia del elemento.devuelve
NULL en caso de error o la lista */
List* list_insertInOrder(List *list, const void *pelem){
	int diferencia=0;
	NodeList *pNodeList=NULL;
	NodeList *pNodeList_next=NULL;
	NodeList *pNodeList_auxiliar=NULL;
	NodeList *pNodeList_auxiliar2=NULL;
	if(list==NULL || pelem==NULL){
	  return NULL;}
	pNodeList=(NodeList*)malloc(sizeof(NodeList));
	pNodeList->next=NULL;
	pNodeList->info=list->copy_element_function(pelem);
	if(list->last==NULL){
		list->last=pNodeList;
		pNodeList->next=pNodeList;
		return list;
	}
	if(list->last->next==list->last){
		diferencia=list->cmp_element_function(pelem, list->last->info);
		if(diferencia<0){
			list->last->next=pNodeList;
			pNodeList->next=list->last;
			return list;
		}
		else{
			pNodeList_auxiliar=list->last;
			list->last=pNodeList;
			pNodeList->next=pNodeList_auxiliar;
			pNodeList_auxiliar->next=pNodeList;
			return list;
		}
	}
	pNodeList_next=list->last;
	pNodeList_auxiliar2=pNodeList_next;
	while(pNodeList_next->next!=pNodeList_auxiliar2){
		diferencia=list->cmp_element_function(pelem, pNodeList_next->next->info);
		if(diferencia<=0){
			pNodeList_auxiliar=pNodeList_next->next;
			pNodeList->next=pNodeList_auxiliar;
			pNodeList_next->next=pNodeList;
			return list;
		}
		pNodeList_next=pNodeList_next->next;
	}
	diferencia=list->cmp_element_function(pelem, pNodeList_auxiliar2->info);
	if(diferencia>=0){
		list_insertLast(list,pNodeList);
		return list;
	}
	else{
		pNodeList->next=pNodeList_auxiliar2;
		pNodeList_next->next=pNodeList;
		return list;
	}
}

/* Extrae del principio de la lista, devolviendo directamente el puntero al
campo info del nodo extraído, nodo que finalmente
es liberado. OJO: tras guardar la dirección del campo info que se va a devolver y antes de liberar el nodo, pone el campo
info del nodo a NULL, para que no siga apuntando a la info a devolver y, por tanto,
no la libere al liberar el nodo */
void * list_extractFirst(List* list){
	NodeList *pNodeList_next=NULL;
	NodeList *pNodeList_auxiliar=NULL;
	void *info=NULL;
	pNodeList_auxiliar=list->last->next->next;
	pNodeList_next=list->last->next;
	info=pNodeList_next->info;
	pNodeList_next->info=NULL;
	pNodeList_next->next=NULL;
	list->last->next=pNodeList_auxiliar;
	free(pNodeList_next);
	return info;
}

/* Extrae del final de la lista, devolviendo directamente el puntero al campo info del nodo extraído, nodo que finalmente es
liberado. OJO: tras guardar la dirección del campo info
que se va a devolver y antes de liberar el nodo, pone el campo info
del nodo a NULL, para que no siga apuntando a la info a devolver y, por tanto, no la libere al liberar el nodo */
void * list_extractLast(List* list){
	NodeList *pNodeList_auxiliar=NULL;
	NodeList *pNodeList_next=NULL;
	void *info=NULL;
	if(list==NULL)
	  return NULL;
	if(list->last==NULL)
	  return NULL;
	if(list->last->next==NULL){
		info=list->last->info;
    list->last->info=NULL;
		list->last->next=NULL;
		free(list->last);
		list->last=NULL;
		return info;
	}
	info=list->last->info;
	list->last->info=NULL;
	pNodeList_auxiliar=list->last->next;
	list->last->next=NULL;
	free(list->last);
	pNodeList_next=pNodeList_auxiliar;
	while(pNodeList_auxiliar!=NULL){
		pNodeList_auxiliar=pNodeList_auxiliar->next;
	}
	list->last=pNodeList_auxiliar;
	pNodeList_auxiliar->next=pNodeList_next;
	return info;
}

/* Comprueba si una lista está vací
a o no.
*/
Bool list_isEmpty(const List* list){

	if (list == NULL) return TRUE;

	if (list->last == NULL) return TRUE;

	return FALSE;
}

/* Devuelve la información almacenada en el nodo i
-
ésimo de la lista.
En caso de error, devuelve NULL. */
const void* list_get(const List* list, int index){
	NodeList *Node_temp = NULL;
	const void *elemento_temp;
	if (list == NULL || index == 0) return NULL;

	Node_temp = list->last->next;


	for(; index>1; index--){
		Node_temp = Node_temp->next;
	}
  elemento_temp=list->copy_element_function(Node_temp->info);
	return elemento_temp;
}

/* Devuelve el número de elementos que hay en una lista.
*/
int list_size (const List* list){
	int size = 0;
	NodeList *elemento_ini = NULL;
	NodeList *elemento_temp = NULL;

	if (list == NULL ) return size;

	elemento_ini = list->last->next;
	elemento_temp = list->last;

	for(size = 1; list->cmp_element_function(elemento_ini->info, elemento_temp->info); size++){
		elemento_ini = elemento_ini->next;
	}

	return size;
}

/* Imprime una lista devolviendo el número de caracteres escritos.
*/
int list_print (FILE *fd, const List* list){

	const void* elemento1=NULL;
	int i = 1, caracteres = 0;
	int tamano=0;

	if(list==NULL){
		fprintf(stderr, "Value of errno: %d\n", errno);
		fprintf(stderr, "Error opening file: %s\n", strerror(errno));
		return 0;
	}
  tamano=list_size(list);
	elemento1 = list_get(list, i);
	while (i<=tamano) {
		caracteres = list->print_element_function(fd, elemento1) + caracteres;
		i++;
	  elemento1 = list_get(list, i);
	}
	return caracteres;
}
