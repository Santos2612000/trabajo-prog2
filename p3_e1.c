#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include "graph.h"
#include "node.h"
#include "types.h"

int main(int argc, char *argv[]){
	Graph *grafo;
  FILE *f;
  f = fopen(argv[1],"r");
  if (argc < 2) {
      fprintf(stderr, "Use: %s <file>\n", argv[0]);
      return 0;
  }
  if (f == NULL) {
   fprintf(stderr,"%s",strerror(errno));
   return 0;
  }
  grafo = graph_ini();
  if (grafo == NULL) {
     fclose(f);
     fprintf(stderr,"%s",strerror(errno));
     return 0;
  }
  graph_readFromFile(f, grafo);
	fclose(f);


  if(graph_findDeepSearch(grafo, 1, 41) == 1){
    printf("SI\n");
  }
	else{
		printf("NO\n");
	}

  graph_destroy(grafo);

	return 0;
}
