#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include "queue.h"


struct _Queue {
  void* items [MAXQUEUE];
  int front;
  int rear;

  destroy_element_function_type   destroy_element_function;
  copy_element_function_type      copy_element_function;
  print_element_function_type     print_element_function;
};


Queue* queue_ini(
    destroy_element_function_type f1,
    copy_element_function_type f2,
    print_element_function_type f3
){

  Queue *pila = NULL;
  int i;

  pila= (Queue *) malloc(sizeof(Queue));

  if (pila==NULL){
    return NULL;
  }

  pila->front = 0;
  pila->rear = 0;

  pila->destroy_element_function = f1;
  pila->copy_element_function = f2;
  pila->print_element_function = f3;

  for(i=0;i<MAXQUEUE;i++){
    pila->items[i]=NULL;
  }

  return pila;

}
void queue_destroy(Queue *q){
  int i;

  if (q!=NULL){
    i= q->front;

    while (i!=q->rear){
      q->destroy_element_function(q->items[i]);
      i= (i+1) % MAXQUEUE;
    }
    free(q);
  }
}

Bool queue_isEmpty(const Queue *q){
  if (q == NULL){
    return FALSE;
  }

  if (q->front == q->rear){
    return TRUE;
  }

  return FALSE;
}


Bool queue_isFull(const Queue* queue){
  if (queue == NULL){
    return FALSE;
  }

  if (queue->front == (queue->rear+1)%MAXQUEUE){
    return TRUE;
  }

  return FALSE;
}

Queue* queue_insert(Queue *q, const void* pElem){

  void* aux = NULL;

  if (q== NULL || pElem== NULL || queue_isFull(q) == TRUE){
    return NULL;
  }

  aux = q->copy_element_function(pElem);
  if (aux == NULL){
    return NULL;
  }
  q->items[q->rear] = aux;

  q->rear=(q->rear+1)%MAXQUEUE;

  return q;

}

void * queue_extract(Queue *q){
  void* pe = NULL;

  if(q == NULL || queue_isEmpty(q) == TRUE) {
    return NULL;
  }

  pe = q->items[q->front];

  q->items[q->front]=NULL;

  q->front=(q->front+1)%MAXQUEUE;

  return pe;

}

int queue_size(const Queue *q){
  int i, cantidad=0;

  for (i = q->front; i!=q->rear; i= (i+1) % MAXQUEUE) cantidad++;
  return cantidad;

}

int queue_print(FILE *pf, const Queue *q){
  int i, caracteres = 0, rear;

  if (q!=NULL){

    for (i = q->front; i!=q->rear; i= (i+1) % MAXQUEUE){
      caracteres = q->print_element_function(pf, q->items[i]) + caracteres;
      fprintf(pf,"\n");
    }
  }

  return caracteres;

}