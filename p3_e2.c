#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include "list.h"

void* int_copy(const void* i);
int int_print(FILE *pf, const void * n);
void int_free(void* i);
int cmp_enteros(const void *entero1, const void *entero2);

int main(int argc, char*argv[]){
  List *lista1=NULL;
  List *lista2=NULL;
  int *entero=NULL;
  if(argc<2){
    return -1;
  }
  entero=(int*)malloc(sizeof(int));
  if(entero==NULL){
    return -1;}
  *entero = atoi(argv[1]);
  if(*entero<1){
    return -1;}
  lista1=list_ini(int_free, int_copy, int_print, cmp_enteros);
  if(lista1==NULL){
    return -1;}
  lista2=list_ini(int_free, int_copy, int_print, cmp_enteros);
  if(lista2==NULL){
    return -1;}
  while((*entero)>=1){
    if((*entero)%2==0){
    lista1=list_insertFirst(lista1, (void*)entero);
    }
    if((*entero)%2==1){
    lista1=list_insertLast(lista1, (void*)entero);
    }
    lista2=list_insertInOrder(lista2, (void*)entero);
    (*entero)=(*entero)-1;

  }
  list_print(stdout, lista1);
  list_destroy(lista1);
  fprintf(stdout, "\n");
  list_print(stdout, lista2);
  list_destroy(lista2);

  return 1;
}


void* int_copy(const void *i){
	int *pEnt = NULL;
  int entero=0;
  pEnt=(int*)malloc(sizeof(int));
  if(pEnt==NULL)
  return NULL;
	entero = *((int*)i);
  *(pEnt)=entero;
	return ((void*)pEnt);
}

int int_print(FILE *pf, const void * n){
	int caracteres=0;
	caracteres = fprintf(pf, " %d ", *((int*)n));
	return caracteres;
}
void int_free(void* i){
  i=NULL;
  return;

}
int cmp_enteros(const void *entero1, const void *entero2){
  int entero;
  int entero3=0;
  int entero4=0;
  entero3=(*(int*)entero1);
  entero4=(*(int*)entero2);
  entero=entero3-entero4;
  return entero;
}
